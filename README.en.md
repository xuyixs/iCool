# iCool

#### 概述
iCool(idea cool)手机是基于合宙Luat项目派生而来，用lua脚本语言开发的5寸触摸屏手机。

#### 功能介绍
iCool手机主要是基于合宙Luat，并且移植LittleVGL而开发的一款手机产品。拥有iCool你可以进行以下操作：  

1. 快速的了解和掌握LittleVGL相关的内容，并且在操作中逐渐熟悉LittleVGL相关控件的功能
2. 内置了时钟，天气，计算器，音频，日历，二维码等的一些软件的内容，满足了手机的一些基本的功能需求
3. 可以自定义的添加iCool手机的内容，让繁琐的app制作变得更加的简单和快捷

#### 使用说明
想要运行iCool手机，你需要
1. 下载 `/main/`， `/resources/`， `/window/`， `/lib/` 目录下的所有代码
2. 把目录 `/core/` 下的固件烧录到iCool手机中，并且同时把上述步骤里的 `lua` 脚本也烧录进硬件中
3. 完成以上2个步骤，即可运行iCool手机了

#### 基本结构
iCool手机基本结构如下：

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210708113216097_%E6%A1%86%E6%9E%B6%E5%9B%BE.png)  

#### 内容预览

<div align="center">
<img src="http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210713145353744_VID_20210713_143111.gif" width = "320" height = "570" alt="控件展示"/>
<img src="http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210713150839500_VID_20210713_150318.gif" width = "320" height = "570" alt="二维码展示"/>  
</div>

#### 授权协议
[MIT License](LICENSE)





