---@diagnostic disable: unused-function, undefined-global
-------------------------------------------------------------------------------------------------------
--1. This file is about the system setting of iCool
--2. Try not to change this file if it is not necessary
-------------------------------------------------------------------------------------------------------

--iCool是否禁用触摸屏
_G.iCool_DisableTp = false

--定义长按关机键的时间变量
local iCool_PowerOffmmSecond = 0
local iCool_PowerOffmSecond = 0
local iCool_PowerOffSecond = 0

--定义是否处于熄屏状态(默认为false)
local iCool_OnSleepScreen = false

--待机熄屏定时器的TimeId
local standByTimeIdOne = nil
local standByTimeIdTwo = nil
--是否是待机熄屏定时器1在启动
local isInStandByTimeOne = true

-- 声音键(点击音量下键进入Boot下载模式)
local function volkey(msg)
    log.info("音乐键 下", msg)
	--音量下键进入Boot下载模式
	ril.request("AT*DOWNLOAD=1")
end

--关机定时函数
local function iCool_PowerOffTiming()
    iCool_PowerOffmmSecond = iCool_PowerOffmmSecond + 1
    if (iCool_PowerOffmmSecond == 5)then
        iCool_PowerOffmSecond = iCool_PowerOffmSecond +1
        iCool_PowerOffmmSecond = 0
    end
    if (iCool_PowerOffmSecond == 10)then
        iCool_PowerOffSecond = iCool_PowerOffSecond + 1
        iCool_PowerOffmSecond = 0
    end
    if (iCool_PowerOffSecond == 5)then
		iCool_PowerOffPopUpInit()
    end
end
--关机回调函数
local function iCool_PowerOff()
	sys.timerLoopStart(iCool_PowerOffTiming, 20, "iCool_PowerOffSignal")
end

--短按熄屏/亮屏
--短暂点击开机键进行熄屏和亮屏
local function iCool_SleepScreen()
	if (iCool_OnSleepScreen)then
		iCool_OnSleepScreen = false
		backlightopen(true)
	else
		iCool_OnSleepScreen = true
		backlightopen(false)
	end
end

--定义开机键功能
--当短暂点击时，是进行熄屏/亮屏操作
--当长按5秒时，是进行关机选择操作
local function keyMsg(msg)
    log.info("开机键",msg.key_matrix_row,msg.key_matrix_col,msg.pressed)
	if msg.pressed then
		iCool_PowerOffmmSecond = 0
		iCool_PowerOffmSecond = 0
		iCool_PowerOffSecond = 0
		if (not iCool_OnSleepScreen)then
			log.info("正在关机中，请按住关机键5秒")
			iCool_PowerOff()
		end
	else
		log.info("你松开手了")
		sys.timerStop(iCool_PowerOffTiming, "iCool_PowerOffSignal")
		iCool_SleepScreen()
		if (_G.iCool_DisableTp)then
			_G.iCool_DisableTp = false
		else
			_G.iCool_DisableTp = true
		end
	end
end

local function iCool_SleepScreenEnd()
	backlightopen(false)
	_G.iCool_DisableTp = true
	iCool_OnSleepScreen = true
end

--待机1分钟，iCool熄屏
function _G.iCool_standByTimeoutSleepScreen()
	if (isInStandByTimeOne)then
		isInStandByTimeOne = false
        sys.timerStop(standByTimeIdTwo)
		standByTimeIdOne = sys.timerStart(iCool_SleepScreenEnd, 60000)
	else
		isInStandByTimeOne = true
        sys.timerStop(standByTimeIdOne)
		standByTimeIdTwo = sys.timerStart(iCool_SleepScreenEnd, 60000)
	end
end

pins.setup(10,volkey,pio.PULLUP)
rtos.closeSoftDog()
rtos.on(rtos.MSG_KEYPAD,keyMsg)
rtos.init_module(rtos.MOD_KEYPAD,0,0,0)