--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "iCool"
--2021.8.19
VERSION = "1.0.4"

--[[
使用Luat物联云平台固件升级的功能，必须按照以下步骤操作：
1、打开Luat物联云平台前端页面：https://iot.openluat.com/
2、如果没有用户名，注册用户
3、注册用户之后，如果没有对应的项目，创建一个新项目
4、进入对应的项目，点击左边的项目信息，右边会出现信息内容，找到ProductKey：把ProductKey的内容，赋值给PRODUCT_KEY变量
]]
PRODUCT_KEY = "sDE9cotCm7os8OgrlHoQuxi0oYbHUh9S"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "pins"
require "net"
--退出下载模式
ril.request("AT*DOWNLOAD=0")
local a, b, c, d, e = pmd.param_get()
log.info("是否有电池", a)
log.info("电压", b)
log.info("电量", c)
log.info("是否在充电", d)
log.info("充电状态", e)
pins.setup(pio.P0_7,0)
if (c < 10)then
	rtos.poweroff()
end
require "Init"

--启动系统框架
sys.init(0, 0)
sys.run()
