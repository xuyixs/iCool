---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

---------------------------------------------Lib库文件----------------------------------------------
require "LCD" --加载LCD
require "tp" --加载tp
require "lvsym"
require "ntp"
require "misc"
require "audio"
require "common"
require "pmd"
require "net"
require "http"
require "update"
-- require "iCoolAppManage"
---------------------------------------------iCool代码----------------------------------------------
require "iCoolBootUp"
require "iCoolPowerOff"
require "iCoolStyle"
require "iCoolIdle"
require "iCoolSystem"
require "iCoolMulti_Widgets"
require "iCoolClock"
require "iCoolFloder"
require "iCoolAudio"
require "iCoolCalendar"
require "iCoolQrCode"
require "iCoolCalculator"
require "iCoolBlueTooth"
require "iCoolWeather"
require "iCoolSetting"
require "iCoolStore"
require "iCoolInterfaceManager"
require "iCoolTouchManager"

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end
	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint
	return data
end

function iCoolInit(id, ispress)
	--显示开机动画
	iCool_BootUpInit()
	-- iCoolAppLunch()
end

local function init()
	lvgl.init(iCoolInit, input)
	pmd.ldoset(7,pmd.LDO_VIBR)
end

--iCool手机初始化
init()

