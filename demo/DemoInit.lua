---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "LCD"
--触摸屏
require "tp"
--LittleVGL内置SYMBOL
require "lvsym"
--------------------------------------------------LittleVGL所有对象的样式文件--------------------------------------------------
require "DemoStyle"
------------------------------------------------------LittleVGL Demo表--------------------------------------------------------
require "Demo_Arc"
require "Demo_Button"
require "Demo_Bar"
require "Demo_Calendar"
require "Demo_CheckBox"
require "Demo_Chart"
require "Demo_Container"
require "Demo_ColorPicker"
require "Demo_DropDown"
require "Demo_Guage"
require "Demo_Image"
require "Demo_ImageButton"
require "Demo_Label"
require "Demo_LED"
require "Demo_List"
require "Demo_LineMeter"
require "Demo_Page"
require "Demo_Roller"
require "Demo_Slider"
require "Demo_SpinBox"
require "Demo_Spinner"
require "Demo_Switch"
require "Demo_Table"
require "Demo_TabView"
require "Demo_Window"
require "Demo_KeyBoard"
--------------------------------------------------LittleVGL暂时不支持的控件--------------------------------------------------------
-- require "Demo_Line"
-- require "Demo_MessageBox"
-- require "Demo_TileView"
--------------------------------------------------LittleVGL所有对象的共同父级---------------------------------------------------
DEMO_BASE_CONT = nil

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	pmd.sleep(100)
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

function demoInit()
	--为Demo创建一个共同的父级容器
	DEMO_BASE_CONT = lvgl.cont_create(lvgl.scr_act(), nil)
	--设置父级容器的大小
	lvgl.obj_set_size(DEMO_BASE_CONT, 480, 854)
	--设置父级容器的位置(对齐方式)
	lvgl.obj_align(DEMO_BASE_CONT, nil, lvgl.ALIGN_CENTER, 0, 0)
	--为父级容器添加样式
	lvgl.obj_add_style(DEMO_BASE_CONT, lvgl.CONT_PART_MAIN, demo_BaseContStyle)

--------------------------------------------------所有控件Demo的初始化函数-----------------------------------------------------------
-----------------------------------------------每一个控件推荐有且仅单独测试一个-----------------------------------------------------------
----------------------------------------------想要演示那个控件就打开相对应的函数-----------------------------------------------------------
	-- demo_ArcInit()
	-- demo_BtnInit()
	-- demo_BarInit()
	-- demo_CalendarInit()
	-- demo_CheckBoxInit()
	-- demo_ChartInit()
	-- demo_ContInit()
	-- demo_CPickerInit()
	-- demo_DropDownInit()
	-- demo_GaugeInit()
	-- demo_ImageInit()
	-- demo_ImageButtonInit()
	-- demo_LabelInit()
	-- demo_LEDlInit()
	-- demo_ListInit()
	-- demo_LineMeterInit()
	-- demo_PageInit()
	-- demo_RollerInit()
	-- demo_SliderInit()
	-- demo_SpinBoxInit()
	-- demo_SpinnerInit()
	-- demo_SwitchInit()
	-- demo_TableInit()
	-- demo_TabViewInit()
	-- demo_WindowInit()
	-- demo_KeyBoardAndTextAreaInit()
end

local function init()
	lvgl.init(demoInit, input)
	pmd.ldoset(8,pmd.LDO_VIBR)
end

sys.taskInit(init, nil)