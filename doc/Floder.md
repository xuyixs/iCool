# Floder界面介绍
[TOC]
## 1. Floder界面：总体结构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Floder界面总体结构分为2个部分，分别为界面总List和界面子List  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
总体结构是由一个 `List` 控件来作为所有文件的父物体，然后由两个相对次一级的子物体来分别表示手机的内部文件和SD卡文件

## 2. Floder界面：界面总List
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
此界面是由一个 `List` 控件来表示的，叫做 `floder_MainList`，然后在其中添加了两个子物体，分别为我的手机和SD卡：

```lua
floder_ListBtn = lvgl.list_add_btn(floder_MainList, lvgl.SYMBOL_DIRECTORY, "我的手机")
floder_ListBtn = lvgl.list_add_btn(floder_MainList, lvgl.SYMBOL_DIRECTORY, "SD卡")
```

## 3. Floder界面：界面子List
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
界面子List中，添加了一个可以返回上一级的按钮 `floder_BackBtn`，可以通过点击次按钮返回上一级，然后在它的下方添加了5个有着不同属性的 `floder_ListBtn`，在其中不仅有着图标显示的文件的类型以及名称还有着所代表的相同的响应事件，作用都是为了打开文件或者进入下一级

具体的代码可以详见Gitee目录：`/window/Floder.lua`

## 4. 开发前后效果图对比
开发前：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625150519714_11.png)  

开发后：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625150056783_Floder.gif)
