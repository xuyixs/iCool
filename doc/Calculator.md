# 计算器界面介绍

## 1. 计算器：总体结构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
计算器界面总体是由2个部分组成，分别为计算器按钮区域 `calcu_BtnControlArea` 和计算器显示区域 `calcu_DisplayResultArea` 。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
总体结构是由一个控件 `CALCULATOR_BASECONT` 作为基容器，在其上分为2个区域，计算器按钮区域主要由一些基本的计算器运算符和数字组成；计算器显示区域是由两个 `label` 控件来作为显示载体，分别为计算过程显示 `calcu_DisplayProcessLabel` 和运算结果显示 `calcu_DisplayLabel` 。

```lua
--计算器显示区域
calcu_DisplayResultArea = lvgl.cont_create(CALCULATOR_BASECONT, nil)
--计算器过程显示
calcu_DisplayProcessLabel = lvgl.label_create(calcu_DisplayResultArea, nil)
--计算器运算结果显示
calcu_DisplayLabel = lvgl.label_create(calcu_DisplayResultArea, nil)
--计算器按钮区域
calcu_BtnControlArea = lvgl.cont_create(CALCULATOR_BASECONT, nil)
```

## 2. 计算器：按钮
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
计算器中的按钮都是通过一个函数 `calcu_AutoCreateBtn` 来自动生成的，通过输入按钮的位置，按钮所对应的响应函数以及名称来便携的生成按钮，具体的代码如下：

```lua
--自动生成按钮
--@pos_x                按钮相对于按钮区域的水平位置
--@pos_y                按钮相对于按钮区域的垂直位置
--@calcu_btnHandle      按钮所对应的响应函数
--@btnName              按钮所显示的名称
local function calcu_AutoCreateBtn(pos_x, pos_y, calcu_btnHandle, btnName)
    calcu_Btn = lvgl.btn_create(calcu_BtnControlArea, nil)
    lvgl.obj_set_size(calcu_Btn, 120, 130)
    lvgl.obj_align(calcu_Btn, calcu_BtnControlArea, lvgl.ALIGN_IN_TOP_LEFT, pos_x, pos_y)
    lvgl.obj_add_style(calcu_Btn, lvgl.BTN_PART_MAIN, calcu_BtnStyle)
    lvgl.obj_set_event_cb(calcu_Btn, calcu_btnHandle)

    calcu_BtnName = lvgl.label_create(calcu_Btn, nil)
    lvgl.label_set_text(calcu_BtnName, btnName)
    lvgl.obj_align(calcu_BtnName, calcu_Btn, lvgl.ALIGN_CENTER, 0, 0)
end
```

## 3. 计算器：运算逻辑
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
计算器的逻辑是仿照Windows自带的计算器中的标准版来进行设计的，当输入运算符时，会检测上次保存的数字来进行运算，并且把运算结果显示到运算结果显示 `calcu_DisplayLabel` 上，并且对于 `=` 号，做了一个重复上次运算的逻辑，即如果是 `2+3=` ，则会显示为 `5` ，然后再次点击 `=` 时，会进行再次 `+3` 的操作

## 4. 效果展示

计划效果展示：
>     

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210708140138960_1.png)

实际效果展示
> 

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210708140143963_2.jpg)


