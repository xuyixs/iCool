# Widgets介绍
Widgets主要分为四个部分
- **Widget** : 控件
- **Benchmark** : 基准
- **WidgetsDisplay** : 部件展示
- **WidgetsHandle()** ： 总体事件处理函数

## 1. Widget
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
widgets界面是展示LVGL中的一些基础的控件所设计的，所以其中包含了大部分的基础控件。在刚设计之初时，我就决定设计一个总的基础框架，然后在其中添加需要添加的控件。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
于是我就在Widgets界面上添加了一个所有界面的父物体的容器，这个容器也就是基础Object，设置的样式为亮灰色，然后在其上添加了一些控件，具体的介绍如下

### 1.1 switch
其控件就是一个switch控件，我为它设计了一个改变界面背景颜色的功能，可以设置背景为白天模式还是夜晚模式，只需要通过开关即可随意切换模式。部分代码如下：

```lua
--switch
wid_sw = lvgl.sw_create(widgetsBase, nil)
lvgl.obj_set_size(wid_sw, 80, 30)
lvgl.obj_align(wid_sw, widgetsBase, lvgl.ALIGN_IN_TOP_RIGHT, -10, 55)
lvgl.sw_set_style(wid_sw, lvgl.SW_STYLE_INDIC, wid_sw_onStyle)
lvgl.obj_set_event_cb(wid_sw, widgetsHandle)
```

### 1.2 dropDown
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
此控件也就是一个可以下拉选项的控件，可以选择其中的选项来显示出来，需要注意的是，不为它设计高度时，此控件是自适应的状态，即选项多少，则高度也就随之改变

```lua
--dropDown
wid_dropDown = lvgl.ddlist_create(widgetsBase, nil)
lvgl.obj_align(wid_dropDown, widgetsBase, lvgl.ALIGN_IN_TOP_MID, 0, 55)
lvgl.ddlist_set_fix_width(wid_dropDown, 150)
lvgl.ddlist_set_options(wid_dropDown, "First\nSecond\nThird")
lvgl.ddlist_set_selected(wid_dropDown, 4);
lvgl.ddlist_set_draw_arrow(wid_dropDown, true)
lvgl.obj_set_event_cb(wid_dropDown, widgetsHandle)
```

### 1.3 TableView
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
这个控件是自带滑屏功能的控件，所以可以很好的综合其他所有的控件，而不需要设计多个界面，节省了内存和降低了复杂度，所以我现阶段设计了三个页面，分别为Control、Visuals、Selector。具体的代码如下：

```lua
--TableView
wid_TableView = lvgl.tabview_create(widgetsBase, nil)
lvgl.obj_align(wid_TableView, widgetsBase, lvgl.ALIGN_IN_TOP_MID, 0, 180)
local temTVStyle = lvgl.cont_get_style(widgetsBase, lvgl.CONT_STYLE_MAIN)
lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BG, temTVStyle)
lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_BG, temTVStyle)
lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_PR, temTVStyle)
lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_REL, temTVStyle)
lvgl.tabview_set_anim_time(wid_TableView, 200)
--在TableView中创建3个页面
wid_firstTab = lvgl.tabview_add_tab(wid_TableView, "Control")
wid_secondTab = lvgl.tabview_add_tab(wid_TableView, "Visuals")
wid_ThirdTab = lvgl.tabview_add_tab(wid_TableView, "Selector")
```

## 4. WidgetsHandle()
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
其中上文中的所有widgetsHandle为widgets所有控件的总的事件处理函数，代码如下：

```lua
--widgets事件处理
local function widgetsHandle(b, e)
	--改变背景颜色
	if (e == lvgl.EVENT_VALUE_CHANGED) then
		if (b == wid_sw)then
			log.info("[Dio-sw]")
			local temStyle = lvgl.style_t()
			temStyle = lvgl.cont_get_style(widgetsBase, lvgl.CONT_STYLE_MAIN)
			if (widgets_BaseColor_white == temStyle) then
			else
				lvgl.cont_set_style(widgetsBase, lvgl.CONT_STYLE_MAIN, widgets_BaseStyle_white)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BG, widgets_BaseStyle_white)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_BG, widgets_BaseStyle_white)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_PR, widgets_BaseStyle_white)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_REL, widgets_BaseStyle_white)
			end
			if (widgets_BaseStyle_black == temStyle) then
			else
				lvgl.cont_set_style(widgetsBase, lvgl.CONT_STYLE_MAIN, widgets_BaseStyle_black)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BG, widgets_BaseStyle_black)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_BG, widgets_BaseStyle_black)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_PR, widgets_BaseStyle_black)
				lvgl.tabview_set_style(wid_TableView, lvgl.TABVIEW_STYLE_BTN_REL, widgets_BaseStyle_black)
			end
		end
		--改变dropDown中的选项
		if (b == wid_dropDown)then
			local buf = nil
			lvgl.ddlist_get_selected_str(wid_dropDown, buf, #buf)
			printf("Option: %s\n", buf);
		end
	end
end
```

> 由于版本从6.1.1升级到了7.1.1所以，今天主要是进行版本的升级，在原有的版本上不仅升级到所需的版本，而且也添加了一些新增加的内容，主要添加了Widgets界面中第二个界面的内容，添加了Line Chart/Line meter/Gauge/Arc/LEDs,具体的效果如下：

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210615204032231_2AE9A54DE1A5C39E3F872D9D35A06BB7.jpg)

--------
--------
--------
> 由于版本的更新，因此代码也随之改变，现在介绍版本从6.1.1更新到7.1.1的代码，具体的设计思路如下：

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
对于Widgets这个app的设计，我观看了网上很多的关于UI设计的展示，发现想要吸引更多人的目光，应该加入一些普遍和炫酷的东西，所以Widgets刚进入app你就会看到一个包含了两个内容的部分，分别是多个控件的综合展示，以及此iCool手机所支持的控件的单个展示，可以很好的让其他开发者去结合iCool手机的特点去进行扩展内容的开发和创造。

## 多控件综合展示
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
排在第一个位置的是多控件综合展示，旨在让开发者可以快速的了解和看到控件在iCool手机上的显示效果，该综合展示借鉴了LittleVGL的官方网站上的控件展示，可以方便开发者进行Lua开发和C语言开发的对比，更好的了解如何用Lua去编写LittleVGL，如下介绍一些核心的代码，方便理解：

此代码是关于如何创建一个变换背景模式的Switch，分为黑夜和白天模式:
```lua
--switch
wid_sw = lvgl.switch_create(multi_WidgetsBase, nil)
lvgl.obj_set_size(wid_sw, 70, 40)
lvgl.obj_align(wid_sw, multi_WidgetsBase, lvgl.ALIGN_IN_TOP_LEFT, 10, 8)
lvgl.obj_add_style(wid_sw, lvgl.SWITCH_PART_INDIC, mulWid_PaddingStyle)
lvgl.obj_set_event_cb(wid_sw, widgetsHandle_switch)
Tab1_cont_label = lvgl.label_create(multi_WidgetsBase, nil)
labelTable[1] = Tab1_cont_label
lvgl.label_set_text(Tab1_cont_label, "Dark")
lvgl.obj_align(Tab1_cont_label, multi_WidgetsBase, lvgl.ALIGN_IN_TOP_LEFT, 90, 20)

```
如下为对应的响应函数：
```lua
--更改主题颜色(黑夜/白天)
local function widgetsHandle_switch(b, e)
	--改变背景颜色
	if (e == lvgl.EVENT_VALUE_CHANGED) then
		if (bool_Bg_Color_isWhite) then
			bool_Bg_Color_isWhite = false

			--TabView的背景颜色(黑夜)
			lvgl.obj_add_style(wid_TableView, lvgl.TABVIEW_PART_BG, widgets_TabStyle_black)
			--Tab标签栏颜色(黑夜)
			lvgl.obj_add_style(wid_TableView, lvgl.TABVIEW_PART_TAB_BG, widgets_BaseStyle_black)
			lvgl.obj_add_style(Tab1_cont, lvgl.CONT_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(Tab1_cont2, lvgl.CONT_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(wid_Tab1_btn1, lvgl.BTN_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(chart, lvgl.CHART_PART_BG, widgets_BaseStyle_black)
			lvgl.obj_add_style(lmeter, lvgl.LINEMETER_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(gauge1, lvgl.GAUGE_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(arc, lvgl.ARC_PART_BG, widgets_BaseStyle_black)
			lvgl.obj_add_style(Tab1_cont3, lvgl.CONT_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(wid_calendar, lvgl.CALENDAR_PART_BG, widgets_BaseStyle_black)
			lvgl.obj_add_style(wid_roller1, lvgl.ROLLER_PART_BG, widgets_BaseStyle_black)
			lvgl.obj_add_style(wid_dropDown2, lvgl.DROPDOWN_PART_MAIN, widgets_BaseStyle_black)
			lvgl.obj_add_style(wid_dropDown2, lvgl.DROPDOWN_PART_LIST, widgets_BaseStyle_black)
			--设置主题字体样式(白)
			lvgl.obj_add_style(Tab1_cont_label_1, lvgl.LABEL_PART_MAIN, fontStyle_White)
			lvgl.obj_add_style(wid_Tab1_ck, lvgl.CHECKBOX_PART_BG, fontStyle_White)
			for k, v in pairs(labelTable) do
				lvgl.obj_add_style(v, lvgl.LABEL_PART_MAIN, fontStyle_White)
			end
		else
			bool_Bg_Color_isWhite = true

			--TabView的整体背景颜色(白天)
			lvgl.obj_add_style(wid_TableView, lvgl.TABVIEW_PART_BG, widgets_TabStyle_white)
			--Tab标签栏颜色(白天)
			lvgl.obj_add_style(wid_TableView, lvgl.TABVIEW_PART_TAB_BG, widgets_BaseStyle_white)
			lvgl.obj_add_style(Tab1_cont, lvgl.CONT_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(Tab1_cont2, lvgl.CONT_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(wid_Tab1_btn1, lvgl.BTN_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(chart, lvgl.CHART_PART_BG, widgets_BaseStyle_white)
			lvgl.obj_add_style(lmeter, lvgl.LINEMETER_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(gauge1, lvgl.GAUGE_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(arc, lvgl.ARC_PART_BG, widgets_BaseStyle_white)
			lvgl.obj_add_style(Tab1_cont3, lvgl.CONT_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(wid_calendar, lvgl.CALENDAR_PART_BG, widgets_BaseStyle_white)
			lvgl.obj_add_style(wid_roller1, lvgl.ROLLER_PART_BG, widgets_BaseStyle_white)
			lvgl.obj_add_style(wid_dropDown2, lvgl.DROPDOWN_PART_MAIN, widgets_BaseStyle_white)
			lvgl.obj_add_style(wid_dropDown2, lvgl.DROPDOWN_PART_LIST, widgets_BaseStyle_white)
			--设置主题字体样式(黑)
			lvgl.obj_add_style(Tab1_cont_label_1, lvgl.LABEL_PART_MAIN, fontStyle_Black)
			lvgl.obj_add_style(wid_Tab1_ck, lvgl.CHECKBOX_PART_BG, fontStyle_Black)
			for k, v in pairs(labelTable) do
				lvgl.obj_add_style(v, lvgl.LABEL_PART_MAIN, fontStyle_Black)
			end
		end
	end
end
```

## 单控件展示
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
在单控件展示中，设计思路是在一个可以滑动的屏幕上，有着iCool所支持的所有控件，若是想要了解哪个控价，即可点击对应的控件容器，即可看到该控件的展示效果，并且使其展示的界面始终处于屏幕的中心位置，如下介绍一些核心代码：
```lua
--说明文字生成以及控件位置设置函数
--@obj          要展示的控件的Object
--@size_x       要展示控件的长度
--@size_y       要展示控件的宽度
--@focusObj     要展示控件的对齐物体Object
--@name         要展示的控件说明
local function sw_labelCreate(obj, size_x, size_y, focusObj, name)
    lvgl.obj_set_top(focusObj, true)
    lvgl.obj_set_size(focusObj, 400, 500)
    lvgl.obj_align(focusObj, disObj, lvgl.ALIGN_IN_BOTTOM_MID, 0, 640)
    if (obj_x ~= 0 and obj_y ~= 0)then
        lvgl.obj_set_size(obj, size_x, size_y)
    end
    lvgl.obj_align(obj, focusObj, lvgl.ALIGN_CENTER, 0, 0)
    sinWid_label = lvgl.label_create(focusObj, nil)
    lvgl.label_set_text(sinWid_label, name)
    lvgl.obj_align(sinWid_label, focusObj, lvgl.ALIGN_IN_TOP_MID, 0, 50)
end
```
> 此代码通过一个函数进行控件的生成以及展示，使其可以快速方便的生成控件的展示和节约时间

```lua
--控件承载坐标容器
disObj = lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_size(disObj, 480, 50)
lvgl.obj_align(disObj, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
lvgl.obj_add_style(disObj, lvgl.CONT_PART_MAIN, disObj_Style)
```
> 此代码保证了展示的界面始终处于屏幕的中心位置，是展示界面的对齐父物体

具体的实现效果如下：
![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210620164313763_%E5%A4%9A%E6%8E%A7%E4%BB%B6%E5%B1%95%E7%A4%BA.gif)