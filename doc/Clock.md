# Clock界面介绍
[TOC]
## 1. Clock界面：总体结构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Clock界面总体的结构主要分为4个部分，分别为闹钟界面、世界时钟界面、秒表界面、定时器界面。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
总体结构主要是由一个TabView控件来作为基础，在TabView中，它添加了4个Tab子物体，分别为clock_AlarmPage、clock_EarthClockPage、clock_StopWatchPage、clock_TimerPage。

```lua
clock_TabView = lvgl.tabview_create(clockPage_Cont, nil)
lvgl.obj_set_size(clock_TabView, 480, 804)
lvgl.obj_align(clock_TabView, clockPage_Cont, lvgl.ALIGN_CENTER, 0, 0)

--Clock界面的4个分界面
clock_AlarmPage = lvgl.tabview_add_tab(clock_TabView, "闹钟")
clock_EarthClockPage = lvgl.tabview_add_tab(clock_TabView, "世界时钟")
clock_StopWatchPage = lvgl.tabview_add_tab(clock_TabView, "秒表")
clock_TimerPage = lvgl.tabview_add_tab(clock_TabView, "定时器")
```

## 2. Clock界面：闹钟界面
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
闹钟界面所有的元素都有着一个共同的父物体 **clock_AlarmPage**，在其中位于中上部分的是一个 `Label` 控件，显示的是具体的时间(时分秒)，在中下部分也同样是一个 `Label` 控件，显示的是(年月日)。为了使时间可以同步的递进，使用到了本公司的内部的函数，获取系统时间：

```lua
misc.getClock()
```

但是此函数只是一次获取系统时间，所以需要以下函数的搭配才可以做到每秒获取一下时间：

```lua
sys.timerLoopStart(fnc, ms, ...)
```

具体的代码可以详见Gitee目录：`/window/Clock.lua`

## 3. Clock界面：世界时钟
暂时未更新，敬请期待

## 4. Clock界面：秒表
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
秒表界面的所有元素都有着一个共同的父物体 **clock_StopWatchPage**，在其中中上部分的是一个 `Label` 控件，显示的是当前的计时的秒表，有着6个显示位数，分别表示分、秒、毫秒；在底部是一个秒表控制容器 `stopWatch_Cont`，在其中有着三个不同的按钮，分别为开始计时按钮、复位时间按钮、分次记录时间按钮；在显示秒表下面的是分次的时间记录，它是一个 `List` 控件，记录的是保存的秒表的时间。

```lua
--显示Label
stopWatch_DisplayLabel = lvgl.label_create(clock_StopWatchPage, nil)
--开始计时按钮
stopWatch_StartBtn = lvgl.btn_create(stopWatch_Cont, nil)
--复位时间按钮
stopWatch_ResetBtn = lvgl.btn_create(stopWatch_Cont, nil)
--分次记录时间按钮
stopWatch_RecordBtn = lvgl.btn_create(stopWatch_Cont, stopWatch_ResetBtn)
--时间记录List
stopWatch_TimeRecordList = lvgl.list_create(clock_StopWatchPage, nil)
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
秒表中的控制按钮主要是如下逻辑进行设计的：当开始按下 `stopWatch_StartBtn` 时，秒表开始计时，同时激活显示 `stopWatch_DisplayLabel`，使其显示秒表计时的数字，并且激活分次记录时间按钮 `stopWatch_RecordBtn`，使其可以通过点击记录时间到时间记录List `stopWatch_TimeRecordList`上，与此同时开始计时按钮的名称也变为暂停计时，当点击时即可暂停计时，与此同时也会关闭分次记录时间按钮的激活状态，使其无法记录时间；当你需要计时结束，即可点击复位按钮来重置显示 `Label`，同时也会清空时间记录List中的数据

具体的代码可以详见Gitee目录：`/window/Clock.lua`

## 5. Clock界面：定时器
暂时未更新，敬请期待

## 6. 开发前后效果图对比
开发前：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625150523221_12.png)  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625150527187_13.png)  

开发后：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625145940334_Clock.gif)