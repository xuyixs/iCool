# iCool主界面框架

## 1. 主界面构成
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
主界面主要是通过对于手机的熟悉和进行针对性的研究过，在几乎所有的手机当中，都是有滑屏这个功能的，所以对于iCool手机而言，也添加了此功能，同时LVGL中也存在一个TableView的构件，它本身也就是具备了滑屏的功能，所以主界面的本体也使用了此构件，但是仅仅是存在一个没有背景的手机，那也显得太过单调，所以添加了以背景为底层基础，在其上添加TableView为其子物体，并且存在一个只显示状态信息的状态栏为子物体的结构，实现了滑动屏幕，只改变TableView中的界面而不会改变状态栏位置，形成了状态栏绝对静止的效果，具体的主界面介绍请看下文所示


主界面主要是由基层容器，容器上方的背景图片，图片上方的TableView构件这三个部分组成。代码如下：

```lua
--设置当前屏幕的容器
SCREAN_MAIN = lvgl.cont_create(nil, nil)

--添加背景图片
mainBackGround = lvgl.img_create(SCREAN_MAIN, nil)
lvgl.img_set_src(mainBackGround, "/lua/wallpaper2.png")
lvgl.obj_set_size(mainBackGround, 480, 854)
lvgl.obj_align(mainBackGround, nil, lvgl.ALIGN_CENTER, 0, 0)

--创建主界面
mainPageTableView = lvgl.tabview_create(mainBackGround, nil)

lvgl.tabview_set_style(mainPageTableView, lvgl.TABVIEW_STYLE_BG, style_TableView)
lvgl.tabview_set_anim_time(mainPageTableView, 200)
lvgl.tabview_set_btns_hidden(mainPageTableView, true)
```

## 2. 主界面App
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
在TableView中，添加了两个 Tab 来表示不同滑动的界面，然后在 Tab 中添加了以控件 imgBtn 为载体的包含各个 App 的结构，其中 imgBtn 为 App 设置了图标、样式、大小等属性，然后在其中又添加了一个子对象 label，用来显示 App 的名称，相对于父物体是绝对静止的。代码如下：

```lua
--在TableView中创建2个页面
firstTab = lvgl.tabview_add_tab(mainPageTableView, "First")
secondTab = lvgl.tabview_add_tab(mainPageTableView, "Second")

--添加了主界面App
---------1. 相机---------
cameraImgBtn = lvgl.imgbtn_create(firstTab, nil)
lvgl.obj_set_size(cameraImgBtn, 100, 100)
lvgl.obj_align(cameraImgBtn, firstTab, lvgl.ALIGN_CENTER, -174, -257)
lvgl.imgbtn_set_src(cameraImgBtn, lvgl.BTN_STATE_REL, "/lua/camera.jpg")
lvgl.imgbtn_set_src(cameraImgBtn, lvgl.BTN_STATE_PR, "/lua/camera.jpg")
lvgl.imgbtn_set_style(cameraImgBtn, lvgl.BTN_STATE_REL, style_InitImgbtn)
lvgl.imgbtn_set_style(cameraImgBtn, lvgl.BTN_STATE_PR, imgbtn_BePressedStyle)

cameraLabel = lvgl.label_create(firstTab, nil)
lvgl.label_set_text(cameraLabel, labelTextTable.camera)
lvgl.obj_align(cameraLabel, cameraImgBtn, lvgl.ALIGN_CENTER, 0, 62)
lvgl.obj_set_event_cb(cameraImgBtn, imgBtnHandle)
```

## 3. 主界面状态
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
主界面状态中主要包含时间、电量、以及蓝牙状态。目前只添加了时间

```lua
--添加状态栏(包含时间、电量、信号、蓝牙)
tableBarimg = lvgl.img_create(mainBackGround, nil)
lvgl.img_set_src(tableBarimg, "/lua/tablebar.png")
lvgl.obj_set_size(tableBarimg, 480, 50)
lvgl.obj_align(tableBarimg, mainBackGround, lvgl.ALIGN_IN_TOP_MID, 0, 0)

bar_ClockLabel = lvgl.label_create(tableBarimg, nil)
lvgl.label_set_style(bar_ClockLabel, lvgl.LABEL_STYLE_MAIN, bar_FontStyle)
--获取到系统时间
tm = misc.getClock()
lvgl.label_set_text(bar_ClockLabel, string.format("%d:%02d", tm.hour,tm.min))
lvgl.obj_align(bar_ClockLabel, tableBarimg, lvgl.ALIGN_IN_LEFT_MID, 8, 0)
```

## 4. 开发前后效果对比
开发前：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625154242123_11.png)

开发后：  

![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210625153945718_6CB4485C63CC7FB02A8B9002E57222A9.gif)