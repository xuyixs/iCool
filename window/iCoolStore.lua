---@diagnostic disable: lowercase-global, undefined-global

--Define a macro for development and debugging
local iCool_storeDebugSignal = true

--app自动生成函数变量
local iCool_storeAppAutoInitVar
--app下载初始化函数变量
local store_appDlBeginVar
--当前商店里的app数量
local iCool_storeCurAppNumber
--商店app容器记录表
local iCool_storeAppTable = {}
--商店app图标记录表
local iCool_storeAppImgTable = {}
--商店app名称记录表
local iCool_storeAppNameTable = {}
--商店app介绍记录表
local iCool_storeAppIntTable = {}
--商店app内存记录表
local iCool_storeAppRamTable = {}
--商店app下载记录表
local iCool_storeAppDlTable = {}

local iCool_storeAppinfoTable = {}
for i = 1, 20 do
    iCool_storeAppinfoTable[i] = {}
    for j = 1, 5 do
        iCool_storeAppinfoTable[i][j] = nil
    end
end

--商店初始化
function iCool_storeInit()
    --商店界面基容器
    STORE_BASECONT = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(STORE_BASECONT, 480, 804)
    lvgl.obj_align(STORE_BASECONT, nil, lvgl.ALIGN_IN_TOP_MID, 0, 50)
    lvgl.obj_add_style(STORE_BASECONT, lvgl.CONT_PART_MAIN, store_scrollPageStyle)
    
    store_mainPage = lvgl.page_create(STORE_BASECONT, nil)
    lvgl.obj_set_size(store_mainPage, lvgl.obj_get_width(STORE_BASECONT), lvgl.obj_get_height(STORE_BASECONT))
    lvgl.obj_align(store_mainPage, STORE_BASECONT, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.page_set_scrollbar_mode(store_mainPage, lvgl.SCROLLBAR_MODE_OFF)
    lvgl.page_set_anim_time(store_mainPage, 50)
    lvgl.obj_add_style(store_mainPage, lvgl.PAGE_PART_BG, store_scrollPageStyle)

    --初始化app数量
    iCool_storeCurAppNumber = 0
    for i = 1, 8 do
        iCool_storeAppAutoInitVar("/lua/Widgets.bin", "微信", "一款跨平台的通讯工具，支持单人、多人参与。", "195.6 MB")
    end
end

--商店页面app自动生成函数  
--@appicon              app的图标路径  
--@appName              app的名称  
--@appInt               app的介绍  
--@appRam               app下载所需的流量  
--@appDlBegin           app下载函数  
local function iCool_storeAppAutoInit(appicon, appName, appInt, appRam)
    iCool_storeCurAppNumber = iCool_storeCurAppNumber + 1
    --App展示容器
    store_appCont = lvgl.cont_create(store_mainPage, nil)
    iCool_storeAppTable[iCool_storeCurAppNumber] = store_appCont
    lvgl.obj_set_size(iCool_storeAppTable[iCool_storeCurAppNumber], 460, 100)
    if (iCool_storeCurAppNumber < 2)then
        lvgl.obj_align(iCool_storeAppTable[iCool_storeCurAppNumber], store_mainPage, lvgl.ALIGN_IN_TOP_MID, 0, 8)
    else
        local cur = iCool_storeCurAppNumber - 1
        lvgl.obj_align(iCool_storeAppTable[iCool_storeCurAppNumber], iCool_storeAppTable[cur], lvgl.ALIGN_OUT_BOTTOM_MID, 0, 8)
    end
    lvgl.obj_set_state(iCool_storeAppTable[iCool_storeCurAppNumber], lvgl.STATE_DISABLED)
    lvgl.obj_add_style(iCool_storeAppTable[iCool_storeCurAppNumber], lvgl.CONT_PART_MAIN, store_appContStyle)
    --app图标
    store_appimg = lvgl.img_create(iCool_storeAppTable[iCool_storeCurAppNumber], nil)
    iCool_storeAppImgTable[iCool_storeCurAppNumber] = store_appimg
    lvgl.img_set_src(iCool_storeAppImgTable[iCool_storeCurAppNumber], appicon)
    lvgl.obj_align(iCool_storeAppImgTable[iCool_storeCurAppNumber], iCool_storeAppTable[iCool_storeCurAppNumber], lvgl.ALIGN_IN_LEFT_MID, 25, 0)
    --app名称
    store_appName = lvgl.label_create(iCool_storeAppTable[iCool_storeCurAppNumber], nil)
    iCool_storeAppNameTable[iCool_storeCurAppNumber] = store_appName
    lvgl.label_set_text(iCool_storeAppNameTable[iCool_storeCurAppNumber], appName)
    lvgl.obj_align(iCool_storeAppNameTable[iCool_storeCurAppNumber], iCool_storeAppImgTable[iCool_storeCurAppNumber], lvgl.ALIGN_OUT_RIGHT_MID, 30, -20)
    --app介绍
    store_appInt = lvgl.label_create(iCool_storeAppTable[iCool_storeCurAppNumber], nil)
    iCool_storeAppIntTable[iCool_storeCurAppNumber] = store_appInt
    lvgl.label_set_text(iCool_storeAppIntTable[iCool_storeCurAppNumber], appInt)
    lvgl.label_set_long_mode(iCool_storeAppIntTable[iCool_storeCurAppNumber], lvgl.LABEL_LONG_SROLL_CIRC)
    lvgl.obj_set_width(iCool_storeAppIntTable[iCool_storeCurAppNumber], 200)
    lvgl.obj_align(iCool_storeAppIntTable[iCool_storeCurAppNumber], iCool_storeAppImgTable[iCool_storeCurAppNumber], lvgl.ALIGN_OUT_RIGHT_MID, 30, 25)
    --app所占内存大小
    store_appRam = lvgl.label_create(iCool_storeAppTable[iCool_storeCurAppNumber], nil)
    iCool_storeAppRamTable[iCool_storeCurAppNumber] = store_appRam
    lvgl.label_set_text(iCool_storeAppRamTable[iCool_storeCurAppNumber], appRam)
    lvgl.obj_align(iCool_storeAppRamTable[iCool_storeCurAppNumber], iCool_storeAppTable[iCool_storeCurAppNumber], lvgl.ALIGN_IN_RIGHT_MID, -30, 25)
    --app下载按钮
    store_appDl = lvgl.btn_create(iCool_storeAppTable[iCool_storeCurAppNumber], nil)
    iCool_storeAppDlTable[iCool_storeCurAppNumber] = store_appDl
    lvgl.obj_set_size(iCool_storeAppDlTable[iCool_storeCurAppNumber], 70, 30)
    lvgl.obj_align(iCool_storeAppDlTable[iCool_storeCurAppNumber], iCool_storeAppTable[iCool_storeCurAppNumber], lvgl.ALIGN_IN_RIGHT_MID, -30, -15)
    lvgl.obj_add_style(iCool_storeAppDlTable[iCool_storeCurAppNumber], lvgl.BTN_PART_MAIN, store_appDlStyle)
    -- lvgl.obj_set_event_cb(iCool_storeAppDlTable[iCool_storeCurAppNumber], appDlBegin)
    --app下载按钮文字
    store_appDlLabel = lvgl.label_create(iCool_storeAppDlTable[iCool_storeCurAppNumber], nil)
    lvgl.label_set_text(store_appDlLabel, "下载")
    lvgl.obj_align(store_appDlLabel, iCool_storeAppDlTable[iCool_storeCurAppNumber], lvgl.ALIGN_CENTER, 0, 0)
end

local function store_appDlBegin(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
    end
end

-------------------自动app生成函数-------------------
iCool_storeAppAutoInitVar = iCool_storeAppAutoInit
-------------------app下载初始化函数------------------
store_appDlBeginVar = store_appDlBegin
