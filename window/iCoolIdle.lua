---@diagnostic disable: undefined-global, lowercase-global
--定义用于开发和调试的宏
local iCool_idleDebugSignal = true

local iCool_AppInitVar = nil
--主界面容器
SCREEN_MAIN = nil
--主界面
mainPageTableView = nil
--主界面的第一个页面
main_FirstPage = nil
--主界面的第二个页面
main_SecondPage = nil
--界面表
--此表储存的是主界面的界面，类似于手机的窗口
pagesTable = {}
--状态栏:时间
mainPage_Bar_Clock = nil
--apps_Widgets
mainPage_appBtn_widgets = nil

local iCoolMainPageBarIsExist = false

--主界面状态栏初始化函数变量
local Idle_StatusBarInitVar = nil
--检测是否插卡
local iCool_SearchSimCardVar = nil
--检测电池
local iCoolCheckBatteryVar = nil
--同步时间
local iCool_SynTimevar = nil
--实时电量检测
local iCool_lockScreenReleaseBatteryVar = nil

local function appHandle_Setting(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Setting")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Setting)
    end
end

local function appHandle_BlueTooth(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]BlueTooth")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_BlueTooth)
    end
end

local function appHandle_Audio(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Audio")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Audio)
    end
end

local function appHandle_QRcode(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]QRcode")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_QrCode)
    end
end

local function appHandle_Calendar(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Calendar")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Calendar)
    end
end

local function appHandle_Store(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Store")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Store)
    end
end

local function appHandle_Floder(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Floder")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Floder)
    end
end

local function appHandle_Calculator(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Calculator")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Calculator)
    end
end

local function appHandle_Weather(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Weather")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Weather)
    end
end

local function appHandle_Clock(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Clock")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Clock)
    end
end

local function appHandle_Camera(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Camera")
    end
end

local function appHandle_Widgets(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Widgets")
        iCool_enterApp(iCool_interfaceIndexTable.interfaceIndex_Widgets)
    end
end


--apps当前数量
appsNumber = 0

--apps信息储存表  
--@string 第一个参数是app的显示名称  
--@number 第二个参数是app的显示长度(默认为62)  
--@number 第三个参数是app的显示宽度(默认为62)  
--@string 第四个参数是app的图标地址(格式为 "/lua/图标名称的bin文件" 也可以是png和jpg格式)  
--@number 第五个参数是app在界面上的位置(相对于TabView中页面的水平距离)  
--@number 第六个参数是app在界面上的位置(相对于TabView中页面的垂直距离)  
--@nil 第七个参数保存的是app的image对象  
--@nil 第八个参数保存的是app名称的Label对象  
--@nil 第九个参数保存的是app事件响应的button对象  
--@function 第十个参数是app事件响应的响应函数  
-----------------------------------------------------
--提示：若要增加自定义参数，可添加在表的后续位置  
--app之间横坐标为:32/150/268/386        (相隔118)  
--app之间纵坐标为:110/228/346/464       (相隔118)  
_G.appsTable =
{
    {"控件", 62, 62, "/lua/Widgets.bin", 32, 110, nil, nil, nil, appHandle_Widgets},
    {"时钟", 62, 62, "/lua/Clock.bin", 150, 110, nil, nil, nil, appHandle_Clock},
    {"天气", 62, 62, "/lua/Weather.bin", 268, 110, nil, nil, nil, appHandle_Weather},
    {"计算器", 62, 62, "/lua/Calculator.bin", 386, 110, nil, nil, nil, appHandle_Calculator},
    {"文件", 62, 62, "/lua/Floder.bin", 32, 228, nil, nil, nil, appHandle_Floder},
    {"音频", 62, 62, "/lua/Audio.bin", 150, 228, nil, nil, nil, appHandle_Audio},
    {"日历", 62, 62, "/lua/Calendar.bin", 268, 228, nil, nil, nil, appHandle_Calendar},
    {"二维码", 62, 62, "/lua/QrCode.bin", 386, 228, nil, nil, nil, appHandle_QRcode},
    {"蓝牙", 62, 62, "/lua/BlueTooth.bin", 32, 346, nil, nil, nil, appHandle_BlueTooth},
    {"设置", 62, 62, "/lua/Widgets.bin", 150, 346, nil, nil, nil, appHandle_Setting},
    {"商店", 62, 62, "/lua/Widgets.bin", 32, 110, nil, nil, nil, appHandle_Store},
    --{"照相", 100, 100, "/lua/Camera.bin", 16, 30, nil, nil, nil, appHandle_Camera},
}

--apps自动布局函数  
--@curAppTable      当前的app存储表  
--@return  
--isChangeScreen    是否添加屏幕  
--nextApp_X         下个app的水平位置  
--nextApp_Y         下个app的垂直位置  
function _G.appsAutoLayout(curAppTable)
    local nextApp_X, nextApp_Y = 0, 0
    local curappNumber = #curAppTable
    local isChangeScreen = false
    if (curappNumber%16 == 0)then
        nextApp_X = 32
        nextApp_Y = 110
        isChangeScreen = true
    else
        if (curAppTable[curappNumber][5] < 386)then
            nextApp_X = curAppTable[curappNumber][5] + 118
            nextApp_Y = curAppTable[curappNumber][6]
        else
            nextApp_X = 32
            nextApp_Y = curAppTable[curappNumber][6] + 118
        end
    end
    return isChangeScreen, nextApp_X, nextApp_Y
end

--apps自动生成函数
local function appsAutoProduce()
    appsNumber = appsNumber + 1
    --定义app显示在那个界面上，此默认显示在界面1上
    local curPage = nil
    if (appsNumber > 10)then
        curPage = pagesTable[2]
    else
        curPage = pagesTable[1]
    end

    --主界面app显示图标
    mainPage_app = lvgl.img_create(curPage, nil)
    _G.appsTable[appsNumber][7] = mainPage_app
    lvgl.img_set_src(_G.appsTable[appsNumber][7], _G.appsTable[appsNumber][4])
    lvgl.obj_set_size(_G.appsTable[appsNumber][7], _G.appsTable[appsNumber][2], _G.appsTable[appsNumber][3])
    lvgl.obj_align(_G.appsTable[appsNumber][7], curPage, lvgl.ALIGN_IN_TOP_LEFT, _G.appsTable[appsNumber][5], _G.appsTable[appsNumber][6])
    lvgl.obj_add_style(_G.appsTable[appsNumber][7], lvgl.IMG_PART_MAIN, absTransStyle)

    --主界面app的名称
	mainPage_appLabel = lvgl.label_create(curPage, nil)
    _G.appsTable[appsNumber][8] = mainPage_appLabel
	lvgl.label_set_text(_G.appsTable[appsNumber][8], _G.appsTable[appsNumber][1])
	lvgl.obj_align(_G.appsTable[appsNumber][8], _G.appsTable[appsNumber][7], lvgl.ALIGN_OUT_BOTTOM_MID, 0, 8)
    lvgl.obj_add_style(_G.appsTable[appsNumber][8], lvgl.LABEL_PART_MAIN, mainPage_IconFontStyle)

    --主界面app的响应事件按钮
    mainPage_appBtn = lvgl.btn_create(_G.appsTable[appsNumber][7], nil)
    _G.appsTable[appsNumber][9] = mainPage_appBtn
    lvgl.obj_set_size(_G.appsTable[appsNumber][9], _G.appsTable[appsNumber][2], _G.appsTable[appsNumber][3])
    lvgl.obj_align(_G.appsTable[appsNumber][9], curPage, lvgl.ALIGN_IN_TOP_LEFT, _G.appsTable[appsNumber][5], _G.appsTable[appsNumber][6])
    lvgl.obj_set_event_cb(_G.appsTable[appsNumber][9], _G.appsTable[appsNumber][10])
    lvgl.obj_add_style(_G.appsTable[appsNumber][9], lvgl.BTN_PART_MAIN, mainPage_IconStyle)
end

function iCool_IdleInit()
    --总界面容器
    SCREEN_MAIN = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(SCREEN_MAIN, 480, 804)
    lvgl.obj_align(SCREEN_MAIN, nil, lvgl.ALIGN_IN_TOP_MID, 0, 50)
    lvgl.obj_add_style(SCREEN_MAIN, lvgl.CONT_PART_MAIN, absTransStyle)

    --手机背景
    mainBackGround1 = lvgl.img_create(SCREEN_MAIN, nil)
    lvgl.img_set_src(mainBackGround1, "/lua/wallpaper021.jpg")
    lvgl.obj_set_size(mainBackGround1, 480, 268)
    lvgl.obj_align(mainBackGround1, SCREEN_MAIN, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    mainBackGround2 = lvgl.img_create(SCREEN_MAIN, nil)
    lvgl.img_set_src(mainBackGround2, "/lua/wallpaper022.jpg")
    lvgl.obj_set_size(mainBackGround2, 480, 268)
    lvgl.obj_align(mainBackGround2, SCREEN_MAIN, lvgl.ALIGN_IN_TOP_MID, 0, 268)

    mainBackGround3 = lvgl.img_create(SCREEN_MAIN, nil)
    lvgl.img_set_src(mainBackGround3, "/lua/wallpaper023.jpg")
    lvgl.obj_set_size(mainBackGround3, 480, 268)
    lvgl.obj_align(mainBackGround3, SCREEN_MAIN, lvgl.ALIGN_IN_TOP_MID, 0, 536)

    --主界面首次生成状态栏(有且仅有一次生成)
    if (not iCoolMainPageBarIsExist)then
        Idle_StatusBarInitVar()
        iCoolMainPageBarIsExist = true
        log.info("iCool", "PageBar Init Success")
    end
    --加载主界面的内容
    iCool_AppInitVar()
end

---------------------------------------------
--优化滑屏
local iCoolPageTranslateVar = nil
---------------------------------------------

--加载app
local function iCool_AppInit()
    --主界面
    mainPageTableView = lvgl.tabview_create(SCREEN_MAIN, nil)
    lvgl.tabview_set_anim_time(mainPageTableView, 10)
    lvgl.obj_set_size(mainPageTableView, 480, 804)
    lvgl.obj_align(mainPageTableView, SCREEN_MAIN, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_add_style(mainPageTableView, lvgl.TABVIEW_PART_BG, absTransStyle)
    lvgl.obj_add_style(mainPageTableView, lvgl.TABVIEW_PART_TAB_BG, absTransStyle)
    
    --主界面添加俩个滑动屏幕
    main_FirstPage = lvgl.tabview_add_tab(mainPageTableView, "")
    main_SecondPage = lvgl.tabview_add_tab(mainPageTableView, "")

    --增加滑屏优化
    -----------------------------------------
    lvgl.obj_set_event_cb(main_FirstPage, iCoolPageTranslateVar)
    lvgl.obj_set_event_cb(main_SecondPage, iCoolPageTranslateVar)
    -----------------------------------------

    lvgl.obj_add_style(main_FirstPage, lvgl.PAGE_PART_SCROLLBAR, absTransStyle)
    lvgl.obj_add_style(main_SecondPage, lvgl.PAGE_PART_SCROLLBAR, absTransStyle)
    pagesTable[1] = main_FirstPage
    pagesTable[2] = main_SecondPage
    
    --遍历循环自动生成app
    for k, v in pairs(_G.appsTable) do
        appsAutoProduce()
    end
    --初始化app数量
    appsNumber = 0
end
--滑屏优化处理
-------------------------------------------
local function iCoolPageTranslate(obj, e)
    iCoolTouchScreenOptimization(e, 10, mainPageTableView)
end
iCoolPageTranslateVar = iCoolPageTranslate
--------------------------------------------

--主界面状态栏初始化
local function Idle_StatusBarInit()
    --主界面状态栏容器
    mainPage_StatusBarCont = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(mainPage_StatusBarCont, 480, 50)
    lvgl.obj_align(mainPage_StatusBarCont, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    lvgl.obj_add_style(mainPage_StatusBarCont, lvgl.CONT_PART_MAIN, absTransStyle)

    mainPage_StatusBarImg = lvgl.img_create(mainPage_StatusBarCont, nil)
    lvgl.img_set_src(mainPage_StatusBarImg, "/lua/StatusBar.jpg")
    lvgl.obj_set_size(mainPage_StatusBarImg, 480, 50)
    lvgl.obj_align(mainPage_StatusBarImg, mainPage_StatusBarCont, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    --状态:时间
    mainPage_Bar_Clock = lvgl.label_create(mainPage_StatusBarCont, nil)
    lvgl.label_set_text(mainPage_Bar_Clock, "00:00")
    lvgl.obj_align(mainPage_Bar_Clock, mainPage_StatusBarImg, lvgl.ALIGN_IN_TOP_MID, 0, 8)
    lvgl.obj_add_style(mainPage_Bar_Clock, lvgl.LABEL_PART_MAIN, defaultFontStyle_White)
    --同步时间
    sys.timerLoopStart(iCool_SynTimevar, 1000)

    --状态:信号
    mainPage_Bar_Internet = lvgl.label_create(mainPage_StatusBarImg, nil)
    lvgl.label_set_text(mainPage_Bar_Internet, "")
    lvgl.obj_align(mainPage_Bar_Internet, mainPage_StatusBarImg, lvgl.ALIGN_IN_LEFT_MID, 10, -10)
    lvgl.obj_add_style(mainPage_Bar_Internet, lvgl.LABEL_PART_MAIN, defaultFontStyle_White)
    --查询sim卡
    iCool_SearchSimCardVar()

    --查询电量
    local iCoolBattery = iCoolCheckBatteryVar()
    --状态:电量
    statusBar_Battery = lvgl.bar_create(mainPage_StatusBarImg, nil)
    lvgl.obj_set_size(statusBar_Battery, 50, 18)
    lvgl.obj_align(statusBar_Battery, mainPage_StatusBarImg, lvgl.ALIGN_IN_RIGHT_MID, -5, -10)
    lvgl.bar_set_anim_time(statusBar_Battery, 1500)
    lvgl.bar_set_range(statusBar_Battery, 0, 100)
    lvgl.bar_set_value(statusBar_Battery, iCoolBattery, lvgl.ANIM_ON)
    lvgl.obj_add_style(statusBar_Battery, lvgl.BAR_PART_BG, statusBar_BatteryBgStyle)
    lvgl.obj_add_style(statusBar_Battery, lvgl.BAR_PART_INDIC, statusBar_BatteryIndicStyle)

    statusBatteryLabel = lvgl.label_create(mainPage_StatusBarImg, nil)
    lvgl.label_set_text(statusBatteryLabel, iCoolBattery.."%")
    lvgl.obj_align(statusBatteryLabel, statusBar_Battery, lvgl.ALIGN_OUT_LEFT_MID, -3, 0)
    lvgl.obj_add_style(statusBatteryLabel, lvgl.LABEL_PART_MAIN, defaultFontStyle_White)

    sys.timerLoopStart(iCool_lockScreenReleaseBatteryVar, 2000)
end

--检测是否插卡
local function iCool_SearchSimCard()
    local iCoolSimExist = sim.getStatus()
    if (iCoolSimExist)then
        print("Sim卡已插入")
        local netModel = net.getNetMode()
        --无网络
        if (netModel == 0)then
            lvgl.label_set_text(mainPage_Bar_Internet, "No Signal")
        --2G GSM网络
        elseif (netModel == 1) then
            lvgl.label_set_text(mainPage_Bar_Internet, "2G")
        --2.5G EDGE数据网络
        elseif (netModel == 2) then
            lvgl.label_set_text(mainPage_Bar_Internet, "2.5G")
        --3G TD网络
        elseif (netModel == 3) then
            lvgl.label_set_text(mainPage_Bar_Internet, "3G")
        --4G LTE网络
        elseif (netModel == 4) then
            lvgl.label_set_text(mainPage_Bar_Internet, "4G")
        --3G WCDMA网络
        elseif (netModel == 5) then
            lvgl.label_set_text(mainPage_Bar_Internet, "3G")
        end
    else
        lvgl.label_set_text(mainPage_Bar_Internet, "No Sim卡")
        print("未检测到Sim")
    end
end

--检测是否充电
local kk = true
local function iCoolCheckBattery()
    local a, b, c, d, e = pmd.param_get()
    if (iCool_idleDebugSignal)then
        log.info("是否有电池", a)
        log.info("电池电压", b)
        log.info("电池电量", c)
        log.info("是否在充电", d)
        log.info("充电状态", e)
    end
    if (kk)then
        pins.setup(pio.P0_7,0)
        kk = false
    end
	if (c == 100)then
		pins.setup(pio.P0_7,1)
	end
    return c
end

--添加锁屏更新电量(对外函数)
local function iCool_lockScreenReleaseBattery()
    local iCoolBattery = iCoolCheckBatteryVar()
    print("-------iiiiiiii-------")
    lvgl.bar_set_value(statusBar_Battery, iCoolBattery, lvgl.ANIM_ON)
    lvgl.label_set_text(statusBatteryLabel, iCoolBattery.."%")
end

--同步时间回调函数
local iCool_InSyncTime = true
local function iCool_SynTime()
	local iCool_getTime = misc.getClock()
    if(iCool_InSyncTime and (sim.getStatus()))then
        print("[SyncTime]", "Sync Time Success")
        ntp.timeSync()
        iCool_InSyncTime = false
    end
    lvgl.label_set_text(mainPage_Bar_Clock, string.format("%02d:%02d", iCool_getTime.hour, iCool_getTime.min))
end

iCool_SearchSimCardVar = iCool_SearchSimCard
iCool_AppInitVar = iCool_AppInit
Idle_StatusBarInitVar = Idle_StatusBarInit
iCoolCheckBatteryVar = iCoolCheckBattery
iCool_SynTimevar = iCool_SynTime
iCool_lockScreenReleaseBatteryVar = iCool_lockScreenReleaseBattery
