---@diagnostic disable: undefined-global, lowercase-global

--Clock界面总容器
clockPage_Cont = nil
--Clock界面的总TabView
clock_TabView = nil
--闹钟界面
clock_AlarmPage = nil
--世界时钟界面
clock_EarthClockPage = nil
--秒表界面
clock_StopWatchPage = nil
--定时器界面
clock_TimerPage = nil
--闹钟界面：添加闹钟按钮
clock_addAlarmBtn = nil
--闹钟界面：闹钟设置界面容器
clock_addAlarmPage_Cont = nil
--闹钟界面：闹钟设置存储表
alarm_SettingTable = {}
--闹钟界面：显示年月日Label
clock_DisplayYearLabel = nil
--闹钟界面：显示时分秒Label
clock_DisplayHourLabel = nil

--秒表界面：时间记录表
stopWatch_TimeRecordTable = {}
--秒表界面：秒表显示Label
stopWatch_DisplayLabel = nil
--秒表界面：秒表开始/暂停按钮Label
stopWatch_StatBtnLabel = nil
--秒表的点击判断变量
stopWatch_IsPause = true
stopWatch_IsReset = false
stopWatch_CanRecord = false
--秒表的显示时间变量
clock_VarTime = 0
clock_TimeAddMinute = 0
clock_TimeAddSecond = 0
clock_TimeAddmSecond = 0
clock_TimeAddmmSecond = 0
--秒表时间记录List
stopWatch_TimeRecordList = nil
--秒表时间记录的个数
stopWatch_TimeRecordTimes = 0


clock_OPA0_Style = lvgl.style_t()
clock_ttStyle = lvgl.style_t()
clockPage_FontStyle = lvgl.style_t()

--闹钟信息表
alarmListTable = {}
for i = 1, 30 do
    alarmListTable[i] = {}
    for j = 1, 2 do
        alarmListTable[i][j] = nil
    end
end
--当前的闹钟数量
clock_CurAddAlarmNumber = 0
--[[
local function addAlarm()
    clock_CurAddAlarmNumber = clock_CurAddAlarmNumber + 1
    clock_addCont = lvgl.cont_create(clock_AlarmPage, nil)
    alarmListTable[clock_CurAddAlarmNumber][1] = clock_addCont
	lvgl.obj_set_size(alarmListTable[clock_CurAddAlarmNumber][1], 440, 70)
    --判断闹钟的对齐方式
    if (clock_CurAddAlarmNumber == 1)then
        log.info("[Clock]--------11111")
        lvgl.obj_align(alarmListTable[clock_CurAddAlarmNumber][1], clock_AlarmPage, lvgl.ALIGN_IN_TOP_MID, 0, 520)
    else
        local curAlarm = clock_CurAddAlarmNumber - 1
        log.info("[Clock]--------22222")
        lvgl.obj_align(alarmListTable[clock_CurAddAlarmNumber][1], alarmListTable[curAlarm][1], lvgl.ALIGN_OUT_BOTTOM_MID, 0, 5)
    end

    clock_AlarmLabel = lvgl.label_create(alarmListTable[clock_CurAddAlarmNumber][1], nil)
    alarmListTable[clock_CurAddAlarmNumber][2] = clock_AlarmLabel
    lvgl.obj_align(alarmListTable[clock_CurAddAlarmNumber][2], alarmListTable[clock_CurAddAlarmNumber][1], lvgl.ALIGN_IN_TOP_LEFT, 30, 20)
    lvgl.label_set_text(alarmListTable[clock_CurAddAlarmNumber][2], "")

    clock_AlarmSwitch = lvgl.switch_create(alarmListTable[clock_CurAddAlarmNumber][1], nil)
    alarmListTable[clock_CurAddAlarmNumber][3] = clock_AlarmSwitch
	lvgl.obj_set_size(alarmListTable[clock_CurAddAlarmNumber][3], 60, 30)
    lvgl.obj_align(alarmListTable[clock_CurAddAlarmNumber][3], alarmListTable[clock_CurAddAlarmNumber][1], lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
end

local function clock_Alarm_Close(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
        lvgl.obj_del(clock_addAlarmPage_Cont)
    end
end
local function clock_Alarm_Create(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
        lvgl.obj_del(clock_addAlarmPage_Cont)
        addAlarm()
    end
end

local function addAlarmPage()
    clock_addAlarmPage_Cont = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(clock_addAlarmPage_Cont, 480, 804)
	lvgl.obj_align(clock_addAlarmPage_Cont, nil, lvgl.ALIGN_IN_TOP_MID, 0, 50)

    --关闭设置闹钟
    clock_CloseAddAlarmPage = lvgl.btn_create(clock_addAlarmPage_Cont, nil)
    lvgl.obj_set_size(clock_CloseAddAlarmPage, 100, 50)
    lvgl.obj_align(clock_CloseAddAlarmPage, clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 10, 10)
    lvgl.obj_set_event_cb(clock_CloseAddAlarmPage, clock_Alarm_Close)

    --完成设置闹钟
    clock_CreateAddAlarmPage = lvgl.btn_create(clock_addAlarmPage_Cont, clock_CloseAddAlarmPage)
    lvgl.obj_align(clock_CreateAddAlarmPage, clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_RIGHT, -10, 10)
    lvgl.obj_set_event_cb(clock_CreateAddAlarmPage, clock_Alarm_Create)

    --设置闹钟时间：时
	clock_Roller_Hour = lvgl.roller_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[1] = clock_Roller_Hour
    lvgl.roller_set_options(alarm_SettingTable[1], "01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12", lvgl.ROLLER_MODE_INFINITE)
    --lvgl.obj_add_style(clock_Roller_Hour, lvgl.ROLLER_PART_BG, clock_OPA0_Style)
    lvgl.obj_set_size(alarm_SettingTable[1], 240, 300)
	lvgl.obj_align(alarm_SettingTable[1], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 50, 80)
    lvgl.roller_set_align(clock_Roller_Hour, lvgl.LABEL_ALIGN_RIGHT)
    lvgl.obj_add_style(clock_Roller_Hour, lvgl.ROLLER_PART_BG, clock_ttStyle)

    --设置闹钟时间：分
    clock_Roller_Minute = lvgl.roller_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[2] = clock_Roller_Minute
    lvgl.roller_set_options(alarm_SettingTable[2], "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12\n13\n14\n15\n"..
                                                    "16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n"..
                                                    "31\n32\n33\n34\n35\n36\n37\n38\n39\n40\n41\n42\n43\n44\n45\n"..
                                                    "46\n47\n48\n49\n50\n51\n52\n53\n54\n55\n56\n57\n58\n59", lvgl.ROLLER_MODE_INFINITE)
    lvgl.obj_set_size(alarm_SettingTable[2], 240, 300)
    lvgl.obj_align(alarm_SettingTable[2], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_RIGHT, -50, 80)
    lvgl.roller_set_align(clock_Roller_Minute, lvgl.LABEL_ALIGN_LEFT)

    --设置闹钟时间：星期
	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[3] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[3], "                                    星期一")
	lvgl.obj_align(alarm_SettingTable[3], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 420)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[4] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[4], "                                    星期二")
	lvgl.obj_align(alarm_SettingTable[4], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 470)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[5] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[5], "                                    星期三")
	lvgl.obj_align(alarm_SettingTable[5], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 520)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[6] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[6], "                                    星期四")
	lvgl.obj_align(alarm_SettingTable[6], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 570)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[7] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[7], "                                    星期五")
	lvgl.obj_align(alarm_SettingTable[7], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 620)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[8] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[8], "                                    星期六")
	lvgl.obj_align(alarm_SettingTable[8], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 670)

	clock_Alarm_ck = lvgl.checkbox_create(clock_addAlarmPage_Cont, nil)
    alarm_SettingTable[9] = clock_Alarm_ck
    lvgl.checkbox_set_text(alarm_SettingTable[9], "                                    星期日")
	lvgl.obj_align(alarm_SettingTable[9], clock_addAlarmPage_Cont, lvgl.ALIGN_IN_TOP_LEFT, 60, 720)

end

local function clock_addAlarmBtnHandle(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[Clock]addButton")
        addAlarmPage()
    end
end
]]

--每秒获取时间
function _G.getTimeOneSec()
    local clock_GetTime = misc.getClock()
    lvgl.label_set_text(clock_DisplayHourLabel, string.format("%02d:%02d:%02d", clock_GetTime.hour, clock_GetTime.min, clock_GetTime.sec))
    lvgl.label_set_text(clock_DisplayYearLabel, string.format("%d年%d月%d日", clock_GetTime.year, clock_GetTime.month, clock_GetTime.day))
end

--秒表界面：秒表显示处理
function _G.clock_StopWatchDisplayHandle()
    clock_VarTime = clock_VarTime + 1
    if (clock_VarTime == 5)then
        clock_TimeAddmSecond = clock_TimeAddmSecond +1
        clock_VarTime = 0
    end
    if (clock_TimeAddmSecond == 10)then
        clock_TimeAddSecond = clock_TimeAddSecond + 1
        clock_TimeAddmSecond = 0
    end
    if (clock_TimeAddSecond == 60)then
        clock_TimeAddMinute = clock_TimeAddMinute + 1
        clock_TimeAddSecond = 0
    end

    clock_TimeAddmmSecond = clock_TimeAddmSecond + 3
    if (clock_TimeAddmmSecond > 9)then
        clock_TimeAddmmSecond = 5
    end
    lvgl.label_set_text(stopWatch_DisplayLabel, string.format("%02d:%02d:%01d%01d", clock_TimeAddMinute, clock_TimeAddSecond, clock_TimeAddmSecond, clock_TimeAddmmSecond))
end

--秒表界面：记录秒表时间
local function clock_RecordStopWatch(obj ,e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[Clock]clock_RecordStopWatch")
        if (stopWatch_CanRecord)then
            stopWatch_TimeRecordTimes = stopWatch_TimeRecordTimes + 1
            stopWatch_ListBtn = lvgl.list_add_btn(stopWatch_TimeRecordList, lvgl.SYMBOL_BELL, string.format("第%d次时间记录:                    %02d:%02d:%01d%01d", 
                                                    stopWatch_TimeRecordTimes, clock_TimeAddMinute, clock_TimeAddSecond, clock_TimeAddmSecond, clock_TimeAddmmSecond))
        end

    end
end

--秒表界面：复位秒表
local function clock_ResetStopWatch(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[Clock]clock_ResetStopWatch")
        sys.timerStop(_G.clock_StopWatchDisplayHandle, "Clock_StopWatch")
        clock_TimeAddMinute = 0
        clock_TimeAddSecond = 0
        clock_TimeAddmSecond = 0
        clock_TimeAddmmSecond = 0
        lvgl.label_set_text(stopWatch_DisplayLabel, string.format("%02d:%02d:%01d%01d", clock_TimeAddMinute, clock_TimeAddSecond, clock_TimeAddmSecond, clock_TimeAddmmSecond))
        lvgl.label_set_text(stopWatch_StatBtnLabel, "开始")
        stopWatch_IsPause = true
        stopWatch_CanRecord = false

        --清除时间记录
        stopWatch_TimeRecordTimes = 0
        lvgl.list_clean(stopWatch_TimeRecordList)
    end
end

--秒表界面：开始/暂停秒表
function clock_StartStopWatch(obj, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[Clock]clock_StartStopWatch")
        if (stopWatch_IsPause)then
            sys.timerLoopStart(_G.clock_StopWatchDisplayHandle, 20, "Clock_StopWatch")
            stopWatch_IsPause = false
            stopWatch_CanRecord = true
            lvgl.label_set_text(stopWatch_StatBtnLabel, "暂停")
        else
            sys.timerStop(_G.clock_StopWatchDisplayHandle, "Clock_StopWatch")
            stopWatch_IsPause = true
            stopWatch_CanRecord = false
            lvgl.label_set_text(stopWatch_StatBtnLabel, "开始")
        end
    end
end

function iCoolTimeInit()
    --Clock界面透明样式
    lvgl.style_init(clock_OPA0_Style)
    lvgl.style_set_bg_opa(clock_OPA0_Style, lvgl.STATE_DEFAULT, lvgl.OPA_0)
    lvgl.style_set_border_opa(clock_OPA0_Style, lvgl.STATE_DEFAULT, lvgl.OPA_0)

    --Clock界面字体样式
    lvgl.style_init(clockPage_FontStyle)
	lvgl.style_set_text_color(clockPage_FontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))

    lvgl.style_init(clock_ttStyle)
	lvgl.style_set_bg_color(clock_ttStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x00FFFF))
    --lvgl.style_set_pad_left(clock_ttStyle, lvgl.STATE_DEFAULT, 150)

    --Clock界面总容器
    clockPage_Cont = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(clockPage_Cont, 480, 804)
	lvgl.obj_align(clockPage_Cont, nil, lvgl.ALIGN_IN_TOP_MID, 0, 50)
    --lvgl.obj_add_style(clockPage_Cont, lvgl.CONT_PART_MAIN, clock_OPA0_Style)

    clock_TabView = lvgl.tabview_create(clockPage_Cont, nil)
	lvgl.obj_set_size(clock_TabView, 480, 804)
	lvgl.obj_align(clock_TabView, clockPage_Cont, lvgl.ALIGN_CENTER, 0, 0)

    --Clock界面的4个分界面
    clock_AlarmPage = lvgl.tabview_add_tab(clock_TabView, "时钟")
    --clock_EarthClockPage = lvgl.tabview_add_tab(clock_TabView, "世界时钟")
    clock_StopWatchPage = lvgl.tabview_add_tab(clock_TabView, "秒表")
    --clock_TimerPage = lvgl.tabview_add_tab(clock_TabView, "定时器")
--[[
    clock_FixCont = lvgl.cont_create(lvgl.scr_act(), nil)
	lvgl.obj_set_size(clock_FixCont, 480, 50)
	lvgl.obj_align(clock_FixCont, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
	lvgl.obj_add_style(clock_FixCont, lvgl.CONT_PART_MAIN, clock_ttStyle)
]]
    --闹钟界面
--[[
    clock_Alarm = lvgl.gauge_create(clock_AlarmPage, nil)
    lvgl.obj_set_size(clock_Alarm, 400, 400)
	lvgl.obj_align(clock_Alarm, clock_AlarmPage, lvgl.ALIGN_IN_TOP_MID, 0, 50)
    lvgl.gauge_set_needle_count(clock_Alarm, 1, lvgl.color_hex(0xFF0000))
    lvgl.gauge_set_range(clock_Alarm, 1, 12)
    lvgl.gauge_set_angle_offset(clock_Alarm, 30)
    lvgl.gauge_set_value(clock_Alarm, 0, 10)

    clock_addAlarmBtn = lvgl.btn_create(clock_AlarmPage, nil)
	lvgl.obj_set_size(clock_addAlarmBtn, 80, 80)
	lvgl.obj_align(clock_addAlarmBtn, clock_AlarmPage, lvgl.ALIGN_IN_TOP_MID, 0, 470)
	lvgl.obj_add_style(clock_addAlarmBtn, lvgl.BTN_PART_MAIN, clock_ttStyle)
    lvgl.obj_set_event_cb(clock_addAlarmBtn, clock_addAlarmBtnHandle)

    clock_addAlarmBtnLabel = lvgl.label_create(clock_addAlarmBtn, nil)
    lvgl.label_set_text(clock_addAlarmBtnLabel, "+")
	lvgl.obj_align(clock_addAlarmBtnLabel, clock_addAlarmBtn, lvgl.ALIGN_CENTER, 0, 0)
]]
    clock_DisplayHourLabel = lvgl.label_create(clock_AlarmPage, nil)
    lvgl.label_set_text(clock_DisplayHourLabel, "00:00:00")
    lvgl.obj_align(clock_DisplayHourLabel, clock_AlarmPage, lvgl.ALIGN_CENTER, 0, -200)

    clock_DisplayYearLabel = lvgl.label_create(clock_AlarmPage, nil)
    lvgl.label_set_text(clock_DisplayYearLabel, "0000年0月0日")
    lvgl.obj_align(clock_DisplayYearLabel, clock_AlarmPage, lvgl.ALIGN_CENTER, 0, 50)
    sys.timerLoopStart(_G.getTimeOneSec, 1000, "getTime")

    --世界时钟界面

    --秒表界面
    stopWatch_DisplayLabel = lvgl.label_create(clock_StopWatchPage, nil)
    lvgl.obj_align(stopWatch_DisplayLabel, clock_StopWatchPage, lvgl.ALIGN_CENTER, -20, -200)
    lvgl.label_set_text(stopWatch_DisplayLabel, string.format("%02d:%02d:%01d%01d", clock_TimeAddMinute, clock_TimeAddSecond, clock_TimeAddmSecond, clock_TimeAddmmSecond))

    stopWatch_Cont = lvgl.cont_create(clock_StopWatchPage, nil)
    lvgl.obj_set_size(stopWatch_Cont, 400, 120)
    lvgl.obj_align(stopWatch_Cont, clock_StopWatchPage, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
    lvgl.obj_add_style(stopWatch_Cont, lvgl.CONT_PART_MAIN, clock_OPA0_Style)

    --开始秒表
    stopWatch_StartBtn = lvgl.btn_create(stopWatch_Cont, nil)
    lvgl.obj_set_size(stopWatch_StartBtn, 90, 90)
    lvgl.obj_align(stopWatch_StartBtn, stopWatch_Cont, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_set_event_cb(stopWatch_StartBtn, clock_StartStopWatch)
    stopWatch_StatBtnLabel = lvgl.label_create(stopWatch_StartBtn, nil)
    lvgl.label_set_text(stopWatch_StatBtnLabel, "开始")
    lvgl.obj_align(stopWatch_StatBtnLabel, stopWatch_StartBtn, lvgl.ALIGN_CENTER, 0, 0)

    --复位秒表
    stopWatch_ResetBtn = lvgl.btn_create(stopWatch_Cont, nil)
    lvgl.obj_set_size(stopWatch_ResetBtn, 60, 60)
    lvgl.obj_align(stopWatch_ResetBtn, stopWatch_Cont, lvgl.ALIGN_IN_LEFT_MID, 30, 5)
    lvgl.obj_add_style(stopWatch_ResetBtn, lvgl.BTN_PART_MAIN, clock_OPA0_Style)
    lvgl.obj_set_event_cb(stopWatch_ResetBtn, clock_ResetStopWatch)
    stopWatch_BtnLabel2 = lvgl.label_create(stopWatch_Cont, nil)
    lvgl.label_set_text(stopWatch_BtnLabel2, "复位")
    lvgl.obj_align(stopWatch_BtnLabel2, stopWatch_ResetBtn, lvgl.ALIGN_CENTER, 0, 0)

    --记录秒表
    stopWatch_RecordBtn = lvgl.btn_create(stopWatch_Cont, stopWatch_ResetBtn)
    lvgl.obj_align(stopWatch_RecordBtn, stopWatch_Cont, lvgl.ALIGN_IN_RIGHT_MID, -30, 5)
    lvgl.obj_set_event_cb(stopWatch_RecordBtn, clock_RecordStopWatch)
    stopWatch_BtnLabel3 = lvgl.label_create(stopWatch_Cont, nil)
    lvgl.label_set_text(stopWatch_BtnLabel3, "记录")
    lvgl.obj_align(stopWatch_BtnLabel3, stopWatch_RecordBtn, lvgl.ALIGN_CENTER, 0, 0)

    --秒表界面：时间记录List
    stopWatch_TimeRecordList = lvgl.list_create(clock_StopWatchPage, nil)
    lvgl.obj_set_size(stopWatch_TimeRecordList, 400, 200)
    lvgl.obj_align(stopWatch_TimeRecordList, clock_StopWatchPage, lvgl.ALIGN_CENTER, 0, 50)
    lvgl.obj_add_style(stopWatch_TimeRecordList, lvgl.LIST_PART_BG, clock_OPA0_Style)

    --定时器界面

end

